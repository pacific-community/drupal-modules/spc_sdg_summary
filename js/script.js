var docwdth = 0;

(function($) {

  // move body on desktop screens

  docwdth =  $(document).width();

  if (docwdth >= 992) {
    var $body = $('.body-notes .dashboard-body');
    var bcont = $body.contents();
    $('#goal-00').html(bcont);
    $body.empty();
    
    $('.sdg-summary dt a').click(function(ev) {
    	var $a = $(this);
    	var gid = $a.data('gid');
    	var newcolor = $a.data('color');
    	var $dl = $a.parents('dl');
    	if (gid == 'goal-00') {
        $dl.removeClass('activated');
    		$dl.find('dt a').removeClass('faded');
    	} else if ($dl.hasClass('activated') && !$a.hasClass('faded')) {
        // already active item
        return true;
      } else {
    		$dl.find('dt a').addClass('faded');
    		$a.removeClass('faded');
        $dl.addClass('activated');
    	}
      // kill link
      ev.preventDefault();
    	// show content
    	$dl.find('dd[id!='+gid+']').fadeOut('250', function() {
    		$dl.find('dd#'+gid).fadeIn('500');	
    	});
    	// update chart
    	$.ajax({
  	    url: Drupal.url('spc_sdg_summary/ajax/'+gid+'/'+$a.data('indicators')),
  	    type: "GET",
  	    contentType: "application/json; charset=utf-8",
  	    dataType: "json",
  	    success: function(response) {
  	      if (response.status == 0) {

            if (dotstat_sdg_summary.options.dotStatChartOptions.countries) {
              const opts = dotstat_sdg_summary.options.dotStatChartOptions.countries;
              var data = [];
              var countries = [];
              for (i=0; i<response.data.length; i++) {
                countries[i] = response.data[i].name;
              }
              for (cidx in opts) {
                const j = countries.indexOf(cidx);
                if (j !== -1) {
                  data.push(response.data[j]);
                } else {
                  data.push({
                    "name": cidx,
                    "label": opts[cidx],
                    "total": 0,
                    "pre2015": 0,
                    "post2015": 0,
                    "y": 0
                  });
                }
              }
              response.data = data;
            }

            $('.sdg-goal-title').text('Indicators with data - '+response.title);
            $('.sdg-goal-count').text(response.indicators+' indicators');

         		dotstat_sdg_summary.update(response.data, { 
              color: newcolor,
              marker: {
                fillColor: newcolor
              },
              zones: [
                {
                  value: 1,
                  color: '#ccc'
                },
                {
                value: 101,
                color: newcolor
                }
              ]
            });
  	       } else {
  	       		$('.sdg-goal-title').text('Indicators with data');
  	       		$('.sdg-goal-count').text('Error loading data');
  	       }
  	    }
  		});
    });
  }
})(jQuery);