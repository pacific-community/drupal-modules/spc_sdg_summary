<?php

namespace Drupal\spc_sdg_summary\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Creates a list of SDG Dashboard Secondary pages.
 *
 * @Block(
 *   id = "spc_sdg_summary",
 *   admin_label = @Translation("SDG Chart Summary"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class SpcSdgSummary extends BlockBase {

  public function defaultConfiguration() {
    
    $conf = <<< EOS
{
  "plotOptions": {
    "series": {
      "pointPadding": 0.1,
      "groupPadding": 0,
      "borderWidth": 1,
      "maxPointWidth": 70,
      "dataLabels": {
        "enabled": false
      },
      "connectorWidth": 3,
      "marker": {
        "radius": 9
      },
      "zoneAxis": "y",
      "zones": [
        {
          "value": 1,
          "color": "#ccc"
        },
        {
          "value": 101,
          "color": "#7cb5ec"
        }
      ]
    }
  },
  "tooltip": {
    "headerFormat": "",
    "pointFormat": "<b>{point.label} - {point.y}%</b><br />{point.total} indicators with data<br />▪ <em>{point.post2015} after 2015</em><br />▪ <em>{point.pre2015} before 2015</em>",
    "outside": true,
    "html": true
  },
  "dotStatChartOptions": {
    "rawDataType": "column",
    "countries": {
      "CK" : "Cook Islands",
      "FJ" : "Fiji",
      "FM" : "Micronesia (Federated States of)",
      "KI" : "Kiribati",
      "MH" : "Marshall Islands",
      "NC" : "New Caledonia",
      "NR" : "Nauru",
      "NU" : "Niue",
      "PF" : "French Polynesia",
      "PG" : "Papua New Guinea",
      "PW" : "Palau",
      "SB" : "Solomon Islands",
      "TO" : "Tonga",
      "TK" : "Tokelau",
      "TV" : "Tuvalu",
      "VU" : "Vanuatu",
      "WS" : "Samoa"
    },
    "goalsIndicators": {
      "0": 131,
      "1": 5,
      "2": 7,
      "3": 16,
      "4": 8,
      "5": 12,
      "6": 3,
      "7": 5,
      "8": 8,
      "9": 3,
      "10": 7,
      "11": 5,
      "12": 4,
      "13": 5,
      "14": 9,
      "15": 6,
      "16": 8,
      "17": 20
    }
  }
}
EOS;
    return [
      'spc_sdg_summary_button' => 'View SDG @idx indicators',
      'spc_sdg_summary_config' => $conf
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // Button title
    $form['spc_sdg_summary_button'] = array(
      '#type' => 'textfield',
      '#title' => t('Buton text'),
      '#size' => 64,
      '#default_value' => $config['spc_sdg_summary_button'],
      '#required' => true
    );
    
    // StatChart and Highchart options
    $form['spc_sdg_summary_config'] = array(
      '#type' => 'textarea',
      '#title' => t('HighCharts Configuration (JSON Object)'),
      '#cols' => 64,
      '#rows' => 12,
      '#default_value' => $config['spc_sdg_summary_config'], 
      '#required' => true
    );

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    
    $this->configuration['spc_sdg_summary_button'] = $values['spc_sdg_summary_button'];
    $this->configuration['spc_sdg_summary_config'] = $values['spc_sdg_summary_config'];

  }
  
  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $conf = $config['spc_sdg_summary_config']; 

    $confparse = empty($conf)?[]:json_decode($conf, true);

    $confgoals = [];
    if (isset($confparse['dotStatChartOptions']['goalsIndicators'])) {
      $confgoals = $confparse['dotStatChartOptions']['goalsIndicators'];
    }

    $confcountries = false;
    if (isset($confparse['dotStatChartOptions']['countries'])) {
      $confcountries = array_keys($confparse['dotStatChartOptions']['countries']);
    }

    // ---------- CONTEXT NODE (PAGE)

     /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getContextValue('node');

    // ---------- OVERALL LINK

    $main = [];

    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('spc_sdg_summary')->getPath();

    $main['link'] = Link::fromTextAndUrl(
      t('<img src="@icon" alt="SDGs in the Pacific" />', [
        '@icon' => '/'.$module_path.'/img/SDGs-in-the-pacific.png']),
      $node->toUrl()->setOption('attributes', [ 
        'data-gid' => 'goal-00',
        'data-color' => '#7cb5ec',
        'data-indicators' => empty($confgoals[0])?0:$confgoals[0]
      ])
    );

    // ---------- DEFAULT CONTENT

    $body = '...';
    /*
    if (!empty($node)) {
      $body = $node->get('body')->processed;
    }
    */

    $main['body'] = $body;

    // ---------- GOALS

    $goals = [];

    $idx = 1;
    foreach ($this->getGoals() as $node) {

      $gid = 'goal-'.str_pad($idx, 2, '0', STR_PAD_LEFT);

      $title = $node->label();
      
      $url = $node->toUrl();

      $color = $node->get('field_dsp_color')->color;
      
      $url->setOption('attributes', [
        'title' => $title,
        // 'role' => 'button',
        // 'target' => '_blank',
        // 'data-toggle' => 'tooltip',
        // 'data-placement' => 'bottom',
        'data-gid' => $gid,
        'data-color' => $color,
        'data-indicators' => empty($confgoals[$idx])?0:$confgoals[$idx]
      ]);

      if (!$node->get('field_dsp_small_icon')->isEmpty()) {
        /** @var \Drupal\file_entity\Entity\FileEntity $file */
        $file = $node->get('field_dsp_small_icon')->entity;
        $icon = $file->createFileUrl(true);

        if ($file->getMimeType() == 'image/svg+xml') {
          $text = new FormattableMarkup("<div style='width:100px; height:100px; display:inline-block'>" . file_get_contents($file->getFileUri()) . "</div>", []);
        } else {
          $text = $this->t('<img src="@icon" alt="Goal #@idx" />', ['@icon' => $icon, '@idx' => $idx]);
        }

      } else {
        $text = $title;
      }

      $link = Link::fromTextAndUrl($text, $url); // ->toRenderable();

      $url2 = $node->toUrl();
      $url2->setOption('attributes', [
       'class' => 'btn btn-dark',
       'style' => 'background-color:'.$color.'; border-color:'.$color
      ]);

      $button = Link::fromTextAndUrl(t($config['spc_sdg_summary_button'], [ '@idx' => $idx ]), $url2);
      
      $goals[] = [
        'gid'   => $gid,
        'link'  => $link,
        'button'=> $button,
        'color' => $color,
        'title' => $node->label(),
        'body'  => $this->stripTagsContent($node->get('body')->processed, '<p><b><strong>')
      ];

      $idx++;
    }

    // ---------- JS & CSS LIBRARIES

    $settings = \Drupal::config('spc_dot_stat_data.settings');

    $libraries = [
      'spc_sdg_summary/spc-sdg-panels', // CSS
      'spc_dot_stat_chart/highcharts', // Highcharts.js library
    ];

    $libraries[] = 'spc_dot_stat_chart/highcharts-more';
    $libraries[] = 'spc_dot_stat_chart/highcharts-lollipop';
    
    $libraries[] = 'spc_dot_stat_chart/statchart';   // Custom JS object calling highchart with submitted parameters
    $libraries[] ='spc_dot_stat_data/dotstatdata';
    
    if ($bootversion = $settings->get('bootstrap')) {
      array_unshift($libraries, 'spc_dot_stat_data/'.$bootversion);
    }

    // echo '<pre>'; print_r($libraries); echo '</pre>';

    // ---------- BUILD

    return [
      '#main' => $main,
      '#goals' => $goals,
      '#chart' => $this->getChart($conf, empty($confgoals[0])?0:$confgoals[0], $confcountries),
      '#theme' => 'spc_sdg_summary',
      '#attached' => [
        'library' => $libraries
      ]
    ];
  }

  /**
   * Return an array of secondary pages
   *
   * @return \Drupal\Core\Entity\EntityInterface[]|\Drupal\taxonomy\Entity\Term[]
   *   A list of terms.
   */
  protected function getGoals() {

    $goals = [];

    $entity_storage = \Drupal::entityTypeManager()->getStorage('node');

    $entity_ids = $entity_storage->getQuery()
      ->condition('type', 'dsp')
      // ->condition('title', 'SDG%', 'LIKE')
      ->sort('field_dsp_view_weight','DESC')
      ->execute();

    foreach ($entity_storage->loadMultiple($entity_ids) as $node) {

      //if ($node->hasTranslation($langcode)) {
      //  $node = $node->getTranslation($langcode);
      //}

      $goals[] = $node;
    }

    return $goals;
  }


  /**
   * remove tags and their content
   */
  protected function stripTagsContent($text, $tags = '', $invert = FALSE) {

    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);
     
    if(is_array($tags) AND count($tags) > 0) {
      if($invert == FALSE) {
        return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
      }
      else {
        return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
      }
    }
    elseif($invert == FALSE) {
      return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
  }

  /**
   * Generate chart
   */
  protected function getChart($conf, $indicount, $confcountries) {

    // ------------ CHART CONFIG

    $template = <<<EOS
{
  "chart": {
    "type": "lollipop",
    "backgroundColor": "#fff",
    "height": 300
  },
  "exporting": {
    "enabled": false
  },
  "xAxis": {
    "type": "category"
  },
  "legend": {
    "enabled": false
  },
  "yAxis": {
    "max": 100,
    "title": {
      "enabled": false
    }
  },
  "credits": {
    "enabled": false
  },
  "dotStatChartOptions": {
  },
  "drilldown": {
  }
}
EOS;

    // --------------- LINK TO .STAT

    $url = '';

    // --------- GET DATA

    $result = spc_sdg_summary_get_goal_data(0, $indicount, $confcountries);

    $rawobj = (object) [
      'data' => $result['data']
    ];

    // ----------------- PREPARE JS

    // generate unique DIV ID attribute
    $did = 'dotstat-sdg-summary';
    
    // JS variable unique name (DotStatChart object)
    $xid = str_replace('-','_', $did);

    // instanciate JS DotStatChart object
    $script = "$xid = new DotStatChart(
      \"$did\", "
      ."\"categories\""
      .", "
      .$template
      .", "
      .$conf
      .", \"\"" // title
      .", \"\"" // subtitle
      .", \"".($url?str_replace('"','',$url):'')."\""
      .");\n";
    
    $script .= "\n$xid.go(".json_encode([ $rawobj ], JSON_NUMERIC_CHECK).");";

    // #($script);
    
    // wait for every external JS to be loaded (Jquery and Highchart) before rendering
    $script = "var $xid = false; window.addEventListener('load', function() {\n".$script."\n});";
    
    // return markup, including :
    // - DIV to append chart in
    // - Aniimated loader image (SVG)
    // - JS code to instanciate DotStatChart object
    return '<div id="'.$did.'">'
      // add animated loader (SVG)
      .'<div class="highcharts-chart">'
      .'<svg viewBox="0 0 135 140" xmlns="http://www.w3.org/2000/svg" class="highcharts-loader"><rect x="0" y="5" width="15" height="140" rx="6"><animate attributeName="height" begin="0s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="30" y="50" width="15" height="100" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="60" y="25" width="15" height="120" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="90;80;70;60;45;30;140;120;120;110;100" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="55;65;75;85;100;115;5;25;25;35;45" calcMode="linear" repeatCount="indefinite" /></rect><rect x="90" y="30" width="15" height="120" rx="6"><animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.25s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect><rect x="120" y="70" width="15" height="80" rx="6"><animate attributeName="height" begin="0.5s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite" /><animate attributeName="y" begin="0.5s" dur="1s" values="25;35;45;55;65;75;85;95;105;5;25" calcMode="linear" repeatCount="indefinite" /></rect></svg>'
      .'</div>'
      // close DIV
      .'</div>'."\n"
      // insert Javascript inline (@todo should go to the end of <body>, after JS includes)
      .'<script>'.$script.'</script>';
    
  }

}
