<?php
namespace Drupal\spc_sdg_summary\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class SummaryChartController extends ControllerBase
{
  // $variable is the wildcard from the route
  public function ajaxCallback($goal, $indicount)
  {
    
    $gid = $goal?intval(substr($goal, 5)):0;

		$ars = spc_sdg_summary_get_goal_data($gid, $indicount);

		$ars['status'] = 0;
	  
	  return new JsonResponse($ars);
  }
}