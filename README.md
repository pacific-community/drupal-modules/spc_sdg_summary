# SPC SDG Summary

Drupal 8/9 module to display a summary of countries indicators with data (as a chart) 

## Installing

### Download using composer

Setup Git repository
```
composer config repositories.spc_sdg_summary vcs \
 git@gitlab.com:pacific-community/drupal-modules/spc_sdg_summary.git
```

Download module
```
composer require pacific_community/spc_sdg_summary:^1.0
```

### Enable module

Through admin GUI or using drush:
```
drush pm-enable spc_sdg_summary
```

### Setup

Add block on page or in block layout.

Currently can only support only one single instance of the chart per page.

## Version History
* 1.0.5: Restrict visualisation to countries listed in configuration only
* 1.0.4: Double click direct access to SDG goal page
* 1.0.3: Fix for Drupal9
* 1.0.2: Video link and animated icon
* 1.0.1: Reponsive and fixes
  * Block responsive for smaller screens (mobile first)
  * Fix ajax call's permissions
* 1.0.0: Initial Release
